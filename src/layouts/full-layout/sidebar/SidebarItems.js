export default [
  { header: "Helpdesk" },
  {
    group: '/users',
    model: false,
    icon: 'mdi-account-circle',
    title: 'Users',
    children: [ 
      {
	icon: 'mdi-adjust',
	title: 'Search',
	to: 'search',
      },
      {
	icon: 'mdi-adjust',
	title: 'Add',
	to: 'add',
      }

    ]
  },
  {
    icon: 'mdi-logout',
    title: 'Logout',
    to: '/logout',
  },
  
]
