import axios from 'axios';

const adaptableAxios = {
  get: function (url, config) {
    if (process.env.NODE_ENV === 'development') {
      url = new URL(url, "http://localhost" + process.env.BASE_URL);
      
      var data = import("@/mock" + url.pathname);
      return data.then(result => {
	return Promise.resolve({data: result});
      });
    } else {
      return axios.get(url,config);
    }
  },
  post: function (url,params,config) {
    //console.log(process.env);
    if (process.env.NODE_ENV === 'development') {
      url = new URL(url, "http://localhost" + process.env.BASE_URL);
      //console.log(url.pathname);
      var data = import("@/mock" + url.pathname);
      return data.then(result => {
	return Promise.resolve({data: result});
      });
    } else {
      /*
      const axios_config = {
        method: 'post',
        url: url,
        headers: config,
	data: params
      }
      
      return axios(axios_config);
      */
      return axios.post(url,params,config);
    }
  }
};

export default adaptableAxios;

